function w = ncols(X)
% WAVEFUN/NCOLS Number of columns in the wave function class
%    w = NCOLS(X) returns the number of columns in the wave function.
%
%    See also Wavefun, nrows.

w = size(X.psi,2);

end
