function c = meDef()
% MEDEF  definition of the mass of an electron
% 
%    See also e2Def.

% Hartree
c = 1;

% Rydberg
% c = 0.5;

end
