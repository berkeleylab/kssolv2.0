function c = e2Def()
% E2DEF  definition of the square of electron charge
% 
%    See also meDef.

% Hartree
c = 1;

% Rydberg
% c = 2;

end
